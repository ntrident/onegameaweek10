using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    [SerializeField] private string sceneOne;
    [SerializeField] private string sceneTwo;
    [SerializeField] private string sceneThree;

    [SerializeField] private Animator transition;

    [SerializeField] private float transitionTime;

    public void LoadLevel_01()
    {
        StartCoroutine(LoadLevel(sceneOne));
    }

    public void LoadLevel_02()
    {
        StartCoroutine(LoadLevel(sceneTwo));
    }

    public void LoadLevel_03()
    {
        StartCoroutine(LoadLevel(sceneThree));
    }

    public IEnumerator LoadLevel(string levelToLoad)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelToLoad);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
