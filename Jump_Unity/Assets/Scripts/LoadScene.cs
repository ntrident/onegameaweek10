using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class LoadScene : MonoBehaviour
{
    [SerializeField] private string sceneNameToLoad;

    [SerializeField] private Animator transition;

    [SerializeField] private float transitionTime;

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(LoadLevel(sceneNameToLoad));
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(sceneNameToLoad));
    }

    private IEnumerator LoadLevel(string levelToLoad)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelToLoad);
    }
}
