using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class ShowHighScore : MonoBehaviour
{
    public TextMeshProUGUI highScoreText_01;
    public TextMeshProUGUI highScoreText_02;
    public TextMeshProUGUI highScoreText_03;

    void Start()
    {

        highScoreText_01.text = "Level 01: " + PlayerPrefs.GetFloat("HighScore_01").ToString("F2");
        highScoreText_02.text = "Level 02: " + PlayerPrefs.GetFloat("HighScore_02").ToString("F2");
        highScoreText_03.text = "Level 03: " + PlayerPrefs.GetFloat("HighScore_03").ToString("F2");
    }
}
