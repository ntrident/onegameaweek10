using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement; 

public class SetHighScore : MonoBehaviour
{
    public float timeStart = 0;
    private bool countingTime = true; 
    public TextMeshProUGUI currentScoreText;
    public TextMeshProUGUI highScoreText;

    void Start()
    {
        currentScoreText.text = "Current Time: " + timeStart.ToString("F2");

        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            highScoreText.text = "HighScore: " + PlayerPrefs.GetFloat("HighScore_01").ToString("F2");
        }

        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            highScoreText.text = "HighScore: " + PlayerPrefs.GetFloat("HighScore_02").ToString("F2");
        }

        if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            highScoreText.text = "HighScore: " + PlayerPrefs.GetFloat("HighScore_03").ToString("F2");
        }

    }

    void Update()
    {
        if(countingTime == true)
        {
            timeStart += Time.deltaTime;
        }

        currentScoreText.text = "Your Time: " + timeStart.ToString("F2");
    }

    private void OnTriggerEnter(Collider other)
    {
        countingTime = false;

        if (SceneManager.GetActiveScene().buildIndex == 3 )
        {
            if (timeStart <= PlayerPrefs.GetFloat("HighScore_01") || PlayerPrefs.GetFloat("HighScore_01") == 0)
            {
                PlayerPrefs.SetFloat("HighScore_01", timeStart);
                highScoreText.text = "HighScore: " + timeStart.ToString("F2");
            }
        }

        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            if (timeStart <= PlayerPrefs.GetFloat("HighScore_02") || PlayerPrefs.GetFloat("HighScore_02") == 0)
            {
                PlayerPrefs.SetFloat("HighScore_02", timeStart);
                highScoreText.text = "HighScore: " + timeStart.ToString("F2");
            }
        }

        if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            if (timeStart <= PlayerPrefs.GetFloat("HighScore_03") || PlayerPrefs.GetFloat("HighScore_03") == 0)
            {
                PlayerPrefs.SetFloat("HighScore_03", timeStart);
                highScoreText.text = "HighScore: " + timeStart.ToString("F2");
            }
        }

    }
}
